import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import {signOut} from 'src/auth/authSlice'
import {store} from 'src/store'

export const API = axios.create({
    baseURL: 'https://evray.ee/app/api',
})

// Member API
export const getMemberCardDetails = (token) => {
    return api.get('/private/member/card', {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    });
};

export const getMemberProfile = (token) => {
    return api.get('/private/member/profile', {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    });
};

export const updateMemberProfile = (token, email, address, telephone) => {
    return api.patch('/private/member/profile', {email, address, telephone}, {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    });
};

export const updateMemberPassword = (token, oldPassword, newPassword) => {
    return api.patch('/private/member/password', {oldPassword, newPassword}, {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    });
};

// export const API = axios.create({
//     baseURL: 'http://localhost:8080/app/api',
// })

const getToken = async () => {
    return await AsyncStorage.getItem('token')
}

const getRefreshToken = async () => {
    return await AsyncStorage.getItem('refresh-token')
}

const showError = (error) => {
    if (Array.isArray(error.response.data)) {
        for (let err in error.response.data) {
            // toast.error(error.response.data[err])
        }
    } else if (error.response.data) {
        // toast.error(error.response.data)
    } else {
        // toast.error('Tundmatu viga')
    }
}

const showSuccess = (response) => {
    if (Array.isArray(response?.response?.data)) {
        for (let err in response.response.data) {
            // toast.success(response.response.data[err])
        }
    } else if (response?.response?.data) {
        // toast.success(response?.response?.data)
    } else if (response?.data) {
        // toast.success(response?.data)
    } else {
        // toast.success('Päring oli edukas')
    }
}

const showWarning = (error) => {
    if (Array.isArray(error.response.data)) {
        for (let war in error.response.data) {
            // toast.warning(error.response.data[war])
        }
    } else if (error?.response?.data?.message) {
        // toast.warning(error.response.data.message)
    } else if (error.response.data) {
        // toast.warning(error.response.data)
    } else {
        // toast.warning('Tundmatu hoiatus!')
    }
}


const requestNewToken = async () => {
    return await API.get('/private/member/auth/refresh', {
        headers: {
            Authorization: `Bearer ${await getRefreshToken()}`
        },
        withCredentials: true
    }).then(result => {
        AsyncStorage.setItem('token', result.data)
        return result.data
    }).catch(error => {
        showError(error)
        if (error.response && (error.response.status === 401 || error.response.status === 403)) {
            store.dispatch(signOut());
            return false
        }
    })
}

const requestInterceptor = async (config) => {
    if (!config.url.includes('/private/member/auth/refresh')) {
        const token = await getToken()
        if (token) {
            config.headers['Authorization'] = `Bearer ${token}`
        }
    }
    return config
}

API.interceptors.request.use(requestInterceptor, (error) => {
    return Promise.reject(error)
})

const responseInterceptor = async (response) => {
    return response
}

let retryCount = 0

const errorInterceptor = async (error) => {
    const originalRequest = error.config
    if (error.response && error.response.status === 401 && !originalRequest._retry && retryCount < 3) {
        originalRequest._retry = true
        retryCount++
        try {
            const newToken = await requestNewToken()
            if (newToken) {
                originalRequest.headers['Authorization'] = 'Bearer ' + newToken
                retryCount = 0
                return API(originalRequest)
            }
        } catch (error) {
            showError(error)
        }
    }

    if (retryCount === 3) {
        store.dispatch(signOut());
        retryCount = 0
    }

    return Promise.reject(error)
}


API.interceptors.response.use(responseInterceptor, errorInterceptor)


export const loginRequest = async (email, password) => {
    return await API.post('/public/member/auth/login', {
        email: email,
        password: password,
    }).then(result => {
        return result
    }).catch(error => {
        return error
    })
}

export const getCard = async () => {
    return await API.get('/private/member/card').then(
        result => {
            return result
        }
    ).catch(error => {
        console.log(error)
        return null
    })
}

export const getProfile = async () => {
    return await API.get('/private/member/profile').then(
        result => {
            return result
        }
    ).catch(error => {
        console.log(error)
        return null
    })
}

export const updatePassword = async (oldPassword, newPassword) => {
    return await API.patch('/private/member/password', {
        oldPassword: oldPassword,
        newPassword: newPassword
    }).then((result) => {
        return result
    }).catch((error) => {
        return error
    })
}

export const resetPasswordRequest = async (email) => {
    return await API.post('/public/member/password/reset', {
        email: email
    }).then((result) => {
        return result
    }).catch((error) => {
        return error
    })
}


