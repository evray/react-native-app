import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import AsyncStorage from '@react-native-async-storage/async-storage'
import {loginRequest} from 'src/services/api'

export const initializeAuth = createAsyncThunk('auth/initializeAuth', async () => {
    const token = await AsyncStorage.getItem('token');
    const refreshToken = await AsyncStorage.getItem('refresh-token');
    const isLoggedIn = !!(token && refreshToken);
    return { token, refreshToken, isLoggedIn };
});

export const login = createAsyncThunk('auth/login', async ({ email, password }, { dispatch }) => {
    const response = await loginRequest(email, password)
    if (response.status !== 200) {
        return response.data
    }
    const data = response.data
    await AsyncStorage.setItem('token', data.token);
    await AsyncStorage.setItem('refresh-token', data.refreshToken);

    dispatch(setCredentials({ token: data.token, refreshToken: data.refreshToken, isLoggedIn: true }));
    return data;
});

export const signOut = createAsyncThunk('auth/signOut', async (_, { dispatch }) => {
    await AsyncStorage.removeItem('token');
    await AsyncStorage.removeItem('refresh-token');
    dispatch(clearCredentials());
});

const authSlice = createSlice({
    name: 'auth',
    initialState: { token: null, refreshToken: null, isLoggedIn: false },
    reducers: {
        setCredentials: (state, action) => {
            const { token, refreshToken, isLoggedIn } = action.payload;
            state.token = token;
            state.refreshToken = refreshToken;
            state.isLoggedIn = isLoggedIn;
        },
        clearCredentials: (state) => {
            state.token = null;
            state.refreshToken = null;
            state.isLoggedIn = false;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(initializeAuth.fulfilled, (state, action) => {
                const { token, refreshToken, isLoggedIn } = action.payload;
                state.token = token;
                state.refreshToken = refreshToken;
                state.isLoggedIn = isLoggedIn;
            });
    },
});

export const { setCredentials, clearCredentials } = authSlice.actions;

export default authSlice.reducer;