import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const MainScreen = () => {
  const navigation = useNavigation();

  const goToLogin = (language) => {
    navigation.navigate('Login', { language });
  };

  return (
    <View style={styles.container}>
      <Image source={require('../../assets/logo_bl.png')} style={styles.logo} />
      <TouchableOpacity
        style={styles.button}
        onPress={() => goToLogin('Estonian')}
      >
        <Text style={styles.buttonText}>Eesti</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.button}
        onPress={() => goToLogin('English')}
      >
        <Text style={styles.buttonText}>English</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.button}
        onPress={() => goToLogin('Russian')}
      >
        <Text style={styles.buttonText}>Русский</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 16,
    backgroundColor: '#FFFFFF',
  },
  logo: {
    alignSelf: 'center',
    marginBottom: 20,
  },
  button: {
    backgroundColor: '#2e6d9d',
    padding: 10,
    marginVertical: 5,
    alignItems: 'center',
    borderRadius: 5,
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 16,
  },
});

export default MainScreen;