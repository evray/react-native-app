import React, { useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet, Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const ChangePasswordScreen = ({ route }) => {
  const { accessToken, refreshToken } = route.params;
  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const navigation = useNavigation();

  const handleChangePassword = async () => {
    if (newPassword !== confirmPassword) {
      Alert.alert('Error', 'New passwords do not match.');
      return;
    }

    try {
      const response = await fetch('http://tootaja.evray.ee/api/private/member/password', {
        method: 'PATCH',
        headers: {
          'Authorization': `Bearer ${accessToken}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ oldPassword, newPassword }),
      });

      if (response.status === 200) {
        Alert.alert('Success', 'Password changed successfully.');
        navigation.navigate('Home', { accessToken });
      } else if (response.status === 498) {
        refreshAccessTokenAndRetry();
      } else {
        Alert.alert('Error', `Error: ${response.status}`);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const refreshAccessTokenAndRetry = async () => {
    try {
      const response = await fetch('http://tootaja.evray.ee/api/private/member/auth/refresh', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ refresh_token: refreshToken }),
      });

      if (response.status === 200) {
        const data = await response.json();
        await AsyncStorage.setItem('accessToken', data.access_token);
        handleChangePassword();
      } else {
        logoutAndRedirectToLogin();
      }
    } catch (error) {
      console.error(error);
    }
  };

  const logoutAndRedirectToLogin = () => {
    navigation.navigate('Login');
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Change Password</Text>
      <TextInput
        style={styles.input}
        placeholder="Old Password"
        secureTextEntry
        value={oldPassword}
        onChangeText={setOldPassword}
      />
      <TextInput
        style={styles.input}
        placeholder="New Password"
        secureTextEntry
        value={newPassword}
        onChangeText={setNewPassword}
      />
      <TextInput
        style={styles.input}
        placeholder="Confirm New Password"
        secureTextEntry
        value={confirmPassword}
        onChangeText={setConfirmPassword}
      />
      <Button
        title="Change Password"
        onPress={handleChangePassword}
        color="#FFFFFF"
      />
      <Button
        title="Cancel"
        onPress={() => navigation.navigate('Home', { accessToken })}
        color="#FFFFFF"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 32,
    backgroundColor: '#004e92',
  },
  title: {
    fontSize: 36,
    color: '#FFFFFF',
    marginBottom: 32,
    textAlign: 'center',
  },
  input: {
    height: 50,
    backgroundColor: '#FFFFFF',
    borderRadius: 4,
    paddingHorizontal: 10,
    marginBottom: 20,
  },
});

export default ChangePasswordScreen;