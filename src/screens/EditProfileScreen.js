import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, Button, StyleSheet, Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const EditProfileScreen = ({ route }) => {
  const { accessToken, refreshToken, email, address, telephone } = route.params;
  const [newEmail, setNewEmail] = useState(email);
  const [newAddress, setNewAddress] = useState(address);
  const [newTelephone, setNewTelephone] = useState(telephone);
  const navigation = useNavigation();

  const handleSaveProfile = async () => {
    if (!newEmail || !newAddress || !newTelephone) {
      Alert.alert('Error', 'Please fill all fields.');
      return;
    }

    if (!isValidEmail(newEmail)) {
      Alert.alert('Error', 'Invalid email address.');
      return;
    }

    try {
      const response = await fetch('http://tootaja.evray.ee/api/private/member/profile', {
        method: 'PATCH',
        headers: {
          'Authorization': `Bearer ${accessToken}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: newEmail,
          address: newAddress,
          telephone: newTelephone,
        }),
      });

      if (response.status === 200) {
        Alert.alert('Success', 'Profile updated successfully.');
        navigation.navigate('Profile', { accessToken });
      } else if (response.status === 498) {
        refreshAccessTokenAndRetry();
      } else {
        Alert.alert('Error', `Error: ${response.status}`);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const refreshAccessTokenAndRetry = async () => {
    try {
      const response = await fetch('http://tootaja.evray.ee/api/private/member/auth/refresh', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ refresh_token: refreshToken }),
      });

      if (response.status === 200) {
        const data = await response.json();
        await AsyncStorage.setItem('accessToken', data.access_token);
        handleSaveProfile();
      } else {
        logoutAndRedirectToLogin();
      }
    } catch (error) {
      console.error(error);
    }
  };

  const logoutAndRedirectToLogin = () => {
    navigation.navigate('Login');
  };

  const isValidEmail = (email) => {
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailPattern.test(email);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Edit Profile</Text>
      <TextInput
        style={styles.input}
        placeholder="Email"
        value={newEmail}
        onChangeText={setNewEmail}
      />
      <TextInput
        style={styles.input}
        placeholder="Address"
        value={newAddress}
        onChangeText={setNewAddress}
      />
      <TextInput
        style={styles.input}
        placeholder="Telephone"
        value={newTelephone}
        onChangeText={setNewTelephone}
      />
      <Button
        title="Save"
        onPress={handleSaveProfile}
        color="#FFFFFF"
      />
      <Button
        title="Cancel"
        onPress={() => navigation.navigate('Profile', { accessToken })}
        color="#FFFFFF"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 32,
    backgroundColor: '#004e92',
  },
  title: {
    fontSize: 36,
    color: '#FFFFFF',
    marginBottom: 32,
    textAlign: 'center',
  },
  input: {
    height: 50,
    backgroundColor: '#FFFFFF',
    borderRadius: 4,
    paddingHorizontal: 10,
    marginBottom: 20,
  },
});

export default EditProfileScreen;