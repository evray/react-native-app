import React, { useState } from 'react';
import { Image, View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { Button, Snackbar, TextInput } from 'react-native-paper';
import { useDispatch } from 'react-redux';
import { login } from 'src/auth/authSlice';
import { useNavigation } from '@react-navigation/native';

export default function LoginScreen() {
    const [visible, setVisible] = useState(false);
    const [snackbarText, setSnackbarText] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const dispatch = useDispatch();
    const navigation = useNavigation();

    const handleLogin = async () => {
        if (email.length <= 1 || password.length <= 1) {
            handleShowSnackbar('Email ja parool on nõutud');
            return;
        }
        const res = await dispatch(login({email, password}))
        if (res["type"] === "auth/login/fulfilled") {
            navigation.navigate('Home');
        } else {
            handleShowSnackbar('Email või parool on vale');
        }
    };

    const handleShowSnackbar = (text) => {
        setSnackbarText(text);
        setVisible(true);
    };

    const handleHideSnackbar = () => {
        setVisible(false);
        setSnackbarText('');
    };

    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Image source={require('../../assets/logo_bl.png')} style={styles.logo} />
            <TextInput
                style={{ marginTop: 10, width: '70%' }}
                label="Email"
                value={email}
                activeUnderlineColor={'#2e6d9d'}
                onChangeText={text => setEmail(text)}
            ></TextInput>
            <TextInput
                style={{ marginTop: 10, width: '70%' }}
                label="Parool"
                activeUnderlineColor={'#2e6d9d'}
                secureTextEntry={true}
                value={password}
                onChangeText={text => setPassword(text)}
            ></TextInput>
            <TouchableOpacity onPress={() => navigation.navigate('PasswordResetEnterEmail')}>
                <Text style={{ marginTop: 10, color: '#2e6d9d' }}>Unustasid Parooli?</Text>
            </TouchableOpacity>
            <Button
                icon="login"
                style={{ backgroundColor: '#2e6d9d', marginTop: 30 }}
                mode="contained"
                onPress={() => handleLogin()}
            >
                Logi sisse
            </Button>
            <Snackbar
                visible={visible}
                style={{ marginBottom: 10 }}
                onDismiss={handleHideSnackbar}
                action={{
                    label: 'Selge',
                    onPress: () => {
                        handleHideSnackbar();
                    },
                }}>
                {snackbarText}
            </Snackbar>
        </View>
    );
}

const styles = StyleSheet.create({
    logo: {
        alignSelf: 'center',
        marginBottom: 20,
      },
});