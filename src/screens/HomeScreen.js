import React, {useState} from 'react'
import {BottomNavigation} from 'react-native-paper'
import {MemberCardScreen} from 'src/screens/MemberCardScreen'
import {SettingsScreen} from 'src/screens/SettingsScreen'


export default function HomeScreen() {

    const [index, setIndex] = useState(0)
    const routes = ([
        {key: 'member_card', title: 'Liikme pilet', focusedIcon: 'ticket-account', unfocusedIcon: 'ticket-outline'},
        {key: 'settings', title: 'Sätted', focusedIcon: 'cogs'}
    ])

    const renderScene = BottomNavigation.SceneMap({
        member_card: MemberCardScreen,
        settings: SettingsScreen,
    })


    return (
        <BottomNavigation
            activeColor={'#2e6d9d'}
            navigationState={{index, routes}}
            onIndexChange={setIndex}
            renderScene={renderScene}
        />
    )
}