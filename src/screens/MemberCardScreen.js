import { Image, StyleSheet, Text, View } from 'react-native'
import { useEffect, useState } from 'react'
import {getCard} from "src/services/api";

export const MemberCardScreen = () => {
    const [data, setData] = useState({
        firstname: '-',
        lastname: '-',
        identificationCode: '-'
    })
    const [currentTime, setCurrentTime] = useState('')

    useEffect(() => {
        const interval = setInterval(() => {
            const now = new Date()
            setCurrentTime(now.toLocaleString('et-EE', {
                hour: '2-digit',
                minute: '2-digit',
                second: '2-digit',
                year: 'numeric',
                month: '2-digit',
                day: '2-digit',
                hour12: false
            }))
        }, 1000)

        return () => clearInterval(interval)
    }, [])

    const fetchData = async () => {
        const res = await getCard()

        if (res?.data) {
            setData(res?.data)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <View style={styles.main_view}>
            <View style={styles.top_view}>
                <View style={[styles.circle, { backgroundColor: data ? '#39FF14' : 'red' }]} />
                {data ? (
                    <Text style={styles.active}>
                        KEHTIV {currentTime}
                    </Text>
                ) : (
                    <Text style={styles.inactive}>
                        KEHTETU {currentTime}
                    </Text>
                )}
            </View>
            <Image source={require('../../assets/logo.png')} style={styles.logo} />
            {!data?.firstname && (
                <Text style={styles.no_connection}>VÕRGUÜHENDUS PUUDUB</Text>
            )}
            <Text style={styles.firstname_lastname}>{data.firstname} {data.lastname}</Text>
            <Text style={styles.identification_code}>{data.identificationCode}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    circle: {
        width: 20,
        height: 20,
        borderRadius: 10,
        marginRight: 5,
    },
    active: {
        color: 'white',
        fontSize: 16
    },
    inactive: {
        color: 'red',
        fontSize: 16
    },
    top_view: {
        flexDirection: 'row',
        alignItems: 'center',
        position: 'absolute',
        top: 10,
        left: 0,
        right: 0,
        justifyContent: 'center',
    },
    main_view: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2e6d9d'
    },
    no_connection: {
        marginTop: 30,
        color: 'red',
        fontSize: 24
    },
    firstname_lastname: {
        marginTop: 30,
        color: 'white',
        fontSize: 24
    },
    identification_code: {
        marginTop: 5,
        color: 'white',
        fontSize: 16
    },
    logo: {
        alignSelf: 'center',
        marginBottom: 20,
      },
})