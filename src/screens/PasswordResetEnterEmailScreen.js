import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Alert, Image } from 'react-native';
import { Button, Snackbar, TextInput } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import {resetPasswordRequest} from "src/services/api";

const PasswordResetEnterEmailScreen = () => {
  const [email, setEmail] = useState('');
  const [visible, setVisible] = useState(false);
  const [snackbarText, setSnackbarText] = useState('');
  const navigation = useNavigation();

  const resetPassword = async () => {
    if (!email) {
      handleShowSnackbar('Palun sisesta email.');
      return;
    }

    const res = await resetPasswordRequest(email);
  };

  const handleShowSnackbar = (text) => {
    setSnackbarText(text);
    setVisible(true);
  };

  const handleHideSnackbar = () => {
    setVisible(false);
    setSnackbarText('');
  };

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Image source={require('../../assets/logo_bl.png')} />
      <TextInput
        style={{ marginTop: 10, width: '70%' }}
        label="Email"
        value={email}
        activeUnderlineColor={'#2e6d9d'}
        onChangeText={text => setEmail(text)}
        autoCapitalize="none"
        keyboardType="email-address"
      />
      <Button
        style={{ backgroundColor: '#2e6d9d', marginTop: 30 }}
        mode="contained"
        onPress={() => resetPassword()}
      >
        Saada kood
      </Button>
      <TouchableOpacity onPress={() => navigation.navigate('PasswordResetChangePassword', { email })}>
        <Text style={{ marginTop: 10, color: '#2e6d9d' }}>Mul on kood olemas</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate('Login')}>
        <Text style={{ marginTop: 10, color: '#2e6d9d' }}>Tagasi</Text>
      </TouchableOpacity>
      <Snackbar
        visible={visible}
        style={{ marginBottom: 10 }}
        onDismiss={handleHideSnackbar}
        action={{
          label: 'Selge',
          onPress: () => {
            handleHideSnackbar();
          },
        }}
      >
        {snackbarText}
      </Snackbar>
    </View>
  );
};

export default PasswordResetEnterEmailScreen;