import React from 'react';
import { View, Text, StyleSheet, Button, Image } from 'react-native';

const ProfileScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <Image source={require('../../assets/logo_bl.png')} style={styles.logo} />
            <Text>Profile Screen</Text>
            <Button title="Go Back to Settings" onPress={() => navigation.navigate('Home', { screen: 'Settings' })} />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
    },
    logo: {
        alignSelf: 'center',
        marginBottom: 20,
        width: 100,
        height: 100,
    },
});

export default ProfileScreen;