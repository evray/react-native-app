import React, { useState } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Button, Snackbar } from 'react-native-paper';
import { useDispatch } from 'react-redux';
import { signOut } from 'src/auth/authSlice';
import { ProfileModal } from 'src/components/ProfileModal';
import { ChangePasswordModal } from 'src/components/ChangePasswordModal';
import {useNavigation} from "@react-navigation/native";

export const SettingsScreen = () => {
    const dispatch = useDispatch();
    const [showProfileModal, setShowProfileModal] = useState(false);
    const [showChangePasswordModal, setShowChangePasswordModal] = useState(false);
    const [visible, setVisible] = useState(false);
    const [snackbarText, setSnackbarText] = useState('');
    const navigation = useNavigation();

    const handleLogout = () => {
        dispatch(signOut());
        navigation.navigate("Main")
    };

    const handleShowSnackbar = (text) => {
        setSnackbarText(text);
        setVisible(true);
    };

    const handleHideSnackbar = () => {
        setVisible(false);
        setSnackbarText('');
    };

    return (
        <View style={styles.container}>
            <Image source={require('../../assets/logo_bl.png')} style={styles.logo} />
            <Button onPress={() => setShowProfileModal(true)} mode="contained" style={styles.button}>
                <Text style={{ fontSize: 16 }}>Profiil</Text>
            </Button>
            <Button onPress={() => setShowChangePasswordModal(true)} mode="contained" style={styles.button}>
                <Text style={{ fontSize: 16 }}>Muuda parool</Text>
            </Button>
            <Button
                mode="contained"
                onPress={handleLogout}
                style={[styles.button, styles.log_out_button]}
            >
                <Text style={{ fontSize: 16 }}>Logi välja</Text>
            </Button>
            <ProfileModal showModal={showProfileModal} setShowModal={setShowProfileModal} />
            <ChangePasswordModal
                showModal={showChangePasswordModal}
                setShowModal={setShowChangePasswordModal}
                handleShowSnackbar={handleShowSnackbar}
            />
            {visible && (
                <Snackbar
                    visible={visible}
                    style={{ marginBottom: 10 }}
                    onDismiss={handleHideSnackbar}
                    action={{
                        label: 'Selge',
                        onPress: () => {
                            handleHideSnackbar();
                        },
                    }}
                >
                    {snackbarText}
                </Snackbar>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    logo: {
        alignSelf: 'center',
        marginBottom: 20,
      },
    button: {
        backgroundColor: '#2e6d9d',
        width: '100%',
        marginVertical: 5,
        paddingVertical: 5,
    },
    log_out_button: {
        marginTop: 50,
    },
});