import React from 'react';
import { View, Button, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const ChangeLanguageScreen = ({ route }) => {
  const { accessToken } = route.params;
  const navigation = useNavigation();

  const changeLanguage = (language) => {
    navigation.navigate('Home', { accessToken, language });
  };

  return (
    <View style={styles.container}>
      <Button
        title="Back"
        onPress={() => navigation.navigate('Home', { accessToken })}
        color="#FFFFFF"
      />
      <Button
        title="Eesti"
        onPress={() => changeLanguage('et')}
        color="#FFFFFF"
      />
      <Button
        title="English"
        onPress={() => changeLanguage('en')}
        color="#FFFFFF"
      />
      <Button
        title="Русский"
        onPress={() => changeLanguage('ru')}
        color="#FFFFFF"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 16,
    backgroundColor: '#004e92',
  },
  button: {
    marginVertical: 8,
  },
});

export default ChangeLanguageScreen;