import React, { useState } from 'react';
import { View, Alert, Image, Text, TouchableOpacity } from 'react-native';
import { Button, Snackbar, TextInput } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

const PasswordResetChangePasswordScreen = ({ route }) => {
  const { email, setEmail } = useState(route.params || '');
  const [resetCode, setResetCode] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [visible, setVisible] = useState(false);
  const [snackbarText, setSnackbarText] = useState('');
  const navigation = useNavigation();

  const changePassword = async () => {
    if (!resetCode || !newPassword || !confirmPassword) {
      handleShowSnackbar('Please fill in all fields.');
      return;
    }

    if (newPassword !== confirmPassword) {
      handleShowSnackbar('Passwords do not match.');
      return;
    }

    try {
      const response = await fetch('http://tootaja.evray.ee/api/public/member/password/reset/code', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, code: resetCode, newPassword }),
      });

      if (response.status === 200) {
        handleShowSnackbar('Password changed successfully.');
        navigation.navigate('Login');
      } else if (response.status === 403) {
        handleShowSnackbar('Wrong reset code.');
      } else {
        handleShowSnackbar('Failed to change password.');
      }
    } catch (error) {
      handleShowSnackbar('Failed to connect to the server.');
      console.error(error);
    }
  };

  const handleShowSnackbar = (text) => {
    setSnackbarText(text);
    setVisible(true);
  };

  const handleHideSnackbar = () => {
    setVisible(false);
    setSnackbarText('');
  };

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Image source={require('../../assets/logo_bl.png')} />
      <TextInput
        style={{ marginTop: 10, width: '70%' }}
        label="Email"
        value={email}
        onChangeText={setEmail}
        activeUnderlineColor={'#2e6d9d'}
      />
      <TextInput
        style={{ marginTop: 10, width: '70%' }}
        label="Kood"
        value={resetCode}
        onChangeText={setResetCode}
        activeUnderlineColor={'#2e6d9d'}
      />
      <TextInput
        style={{ marginTop: 10, width: '70%' }}
        label="Uus parool"
        value={newPassword}
        onChangeText={setNewPassword}
        secureTextEntry
        activeUnderlineColor={'#2e6d9d'}
      />
      <TextInput
        style={{ marginTop: 10, width: '70%' }}
        label="Kinnita uus parool"
        value={confirmPassword}
        onChangeText={setConfirmPassword}
        secureTextEntry
        activeUnderlineColor={'#2e6d9d'}
      />
      <Button
        icon="lock-reset"
        style={{ backgroundColor: '#2e6d9d', marginTop: 30 }}
        mode="contained"
        onPress={changePassword}
      >
        Vaheta parool
      </Button>
      <TouchableOpacity onPress={() => navigation.navigate('PasswordResetEnterEmail')}>
        <Text style={{ marginTop: 10, color: '#2e6d9d' }}>Tagasi</Text>
      </TouchableOpacity>
      <Snackbar
        visible={visible}
        style={{ marginBottom: 10 }}
        onDismiss={handleHideSnackbar}
        action={{
          label: 'Selge',
          onPress: () => {
            handleHideSnackbar();
          },
        }}
      >
        {snackbarText}
      </Snackbar>
    </View>
  );
};

export default PasswordResetChangePasswordScreen;