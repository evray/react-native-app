import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import MainScreen from 'src/screens/MainScreen';
import LoginScreen from 'src/screens/LoginScreen';
import HomeScreen from 'src/screens/HomeScreen';
import ChangePasswordScreen from 'src/screens/ChangePasswordScreen';
import ChangeLanguageScreen from 'src/screens/ChangeLanguageScreen';
import EditProfileScreen from 'src/screens/EditProfileScreen';
import ProfileScreen from 'src/screens/ProfileScreen';
import PasswordResetEnterEmailScreen from 'src/screens/PasswordResetEnterEmailScreen';
import PasswordResetChangePasswordScreen from 'src/screens/PasswordResetChangePasswordScreen';

const Stack = createNativeStackNavigator();

const Layout = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name="Main" component={MainScreen} />
                <Stack.Screen name="Login" component={LoginScreen} />
                <Stack.Screen name="Home" component={HomeScreen} />
                <Stack.Screen name="ChangePassword" component={ChangePasswordScreen} />
                <Stack.Screen name="ChangeLanguage" component={ChangeLanguageScreen} />
                <Stack.Screen name="EditProfile" component={EditProfileScreen} />
                <Stack.Screen name="Profile" component={ProfileScreen} />
                <Stack.Screen name="PasswordResetEnterEmail" component={PasswordResetEnterEmailScreen} />
                <Stack.Screen name="PasswordResetChangePassword" component={PasswordResetChangePasswordScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default Layout;