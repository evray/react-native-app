import {StyleSheet} from 'react-native'
import {Button, Modal, TextInput} from 'react-native-paper'
import {useState} from 'react'
import {updatePassword} from 'src/services/api'


export const ChangePasswordModal = ({showModal, setShowModal, handleShowSnackbar}) => {
    const containerStyle = {backgroundColor: 'white', padding: 20}
    const [password, setPassword] = useState('')
    const [repeatPassword, setRepeatPassword] = useState('')
    const [oldPassword, setOldPassword] = useState('')

    const handleSave = async () => {
        if (oldPassword <= 1) {
            handleShowSnackbar("Vana parool ei tohi olla tühi")
            return
        }

        if (password < 8 || repeatPassword.length < 8) {
            handleShowSnackbar("Parool peab olema vähemalt 8 märki")
            return
        }

        if (password !== repeatPassword) {
            handleShowSnackbar("Paroolid ei kattu")
            return
        }

        const res = await updatePassword(oldPassword, password)

        if (res?.status === 200) {
            handleShowSnackbar('Parool edukalt uuendatud')
            setPassword('')
            setRepeatPassword('')
            setOldPassword('')

            setShowModal(false)
            return
        }

        if (res?.response?.data) {
            handleShowSnackbar(res.response.data)
        } else {
            handleShowSnackbar('Tundmatu viga')
        }

    }

    return (
        <Modal visible={showModal} onDismiss={() => setShowModal(false)} contentContainerStyle={containerStyle}>
            <TextInput
                style={{marginTop: 10}}
                label="Vana parool"
                activeUnderlineColor={'#2e6d9d'}
                secureTextEntry={true}
                value={oldPassword}
                onChangeText={text => setOldPassword(text)}
            ></TextInput>
            <TextInput
                style={{marginTop: 20}}
                label="Uus parool"
                activeUnderlineColor={'#2e6d9d'}
                secureTextEntry={true}
                value={password}
                onChangeText={text => setPassword(text)}
            ></TextInput>
            <TextInput
                style={{marginTop: 10}}
                label="Korda parool"
                activeUnderlineColor={'#2e6d9d'}
                secureTextEntry={true}
                value={repeatPassword}
                onChangeText={text => setRepeatPassword(text)}
            ></TextInput>

            <Button icon="content-save"
                    style={style.save_button}
                    mode="contained"
                    onPress={() => handleSave()}>
                Salvesta
            </Button>

            <Button icon="arrow-left-circle"
                    style={style.back_button}
                    mode="contained"
                    onPress={() => setShowModal(false)}>
                Tagasi
            </Button>

        </Modal>
    )
}

const style = StyleSheet.create({
    save_button: {
        backgroundColor: 'green',
        marginTop: 30
    },
    back_button: {
        backgroundColor: '#2e6d9d',
        marginTop: 30
    }
})