import { Linking, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { Button, Modal } from 'react-native-paper';
import { useEffect, useState } from 'react';
import {getProfile} from "src/services/api";

export const ProfileModal = ({ showModal, setShowModal }) => {
    const containerStyle = { backgroundColor: 'white', padding: 20 };
    const [data, setData] = useState({});

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        const res = await getProfile()
        setData(res?.data)
    }

    const formattedDate = (date) => {
        if (!date) {
            return '';
        }

        const d = new Date(date);
        return d.toLocaleDateString('et-EE', {
            day: '2-digit',
            month: '2-digit',
            year: 'numeric',
        });
    };

    const handleEmailPress = () => {
        Linking.openURL('mailto:muuda@evray.ee');
    };

    return (
        <Modal visible={showModal} onDismiss={() => setShowModal(false)} contentContainerStyle={containerStyle}>
            <Text style={style.text}>Nimi: {data.firstname} {data.lastname}</Text>
            <Text style={style.text}>Isikukood: {data.identificationCode}</Text>
            <Text style={style.text}>Sünnikuupäev: {formattedDate(data.dateOfBirth)}</Text>
            <Text style={style.text}>Email: {data.email}</Text>
            <Text style={style.text}>Telefon: {data.telephone}</Text>
            <Text style={style.text}>Aadress: {data.address}</Text>
            <Text style={style.text}>Sisseastumine: {formattedDate(data.dateOfEntry)}</Text>
            <Text style={style.text}>Töökoht: {data.companyName}</Text>
            <TouchableOpacity onPress={handleEmailPress}>
                <Text style={style.contact}>Andmete muutmiseks palun kirjutage <Text style={{ color: '#0000EE' }}>muuda@evray.ee</Text></Text>
            </TouchableOpacity>

            <Button
                icon="arrow-left-circle"
                style={{ backgroundColor: '#2e6d9d', marginTop: 30 }}
                mode="contained"
                onPress={() => setShowModal(false)}
            >
                <Text>Tagasi</Text>
            </Button>
        </Modal>
    );
};

const style = StyleSheet.create({
    text: {
        margin: 5,
        fontSize: 16,
    },
    contact: {
        marginTop: 20,
        fontSize: 14,
        color: 'grey',
    },
});