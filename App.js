import React, {useEffect} from 'react'
import {Provider as PaperProvider} from 'react-native-paper'
import theme from './src/config/theme'
import {Provider as ReduxProvider} from 'react-redux'
import {store} from 'src/store'
import {initializeAuth} from 'src/auth/authSlice'
import Layout from 'src/Layout'
import {SafeAreaView, StatusBar, StyleSheet} from 'react-native'

export default function App() {
    useEffect(() => {
        store.dispatch(initializeAuth())
    }, [])


    return (
        <ReduxProvider store={store}>
            <PaperProvider theme={theme}>
                <SafeAreaView style={styles.container}>
                    <StatusBar barStyle="dark-content" backgroundColor="#fff" />
                    <Layout />
                </SafeAreaView>
            </PaperProvider>
        </ReduxProvider>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
    },
});